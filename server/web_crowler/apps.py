from django.apps import AppConfig


class WebCrowlerConfig(AppConfig):
    name = 'web_crowler'
