from django.conf.urls import url

from .views import *

urlpatterns = [
    url('^$', index, name='index'),
    url('^get-page$', get_page, name='get_page'),
    url('^db', show_db, name='show_db'),
    url('^new_view$', new_view, name='new_view'),
]
