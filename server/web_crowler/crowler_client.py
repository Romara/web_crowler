from selenium import webdriver
import hashlib

from .constants import *
from .models import Page
from .utils import extract_domain, check_url
from urllib.error import URLError


class Crowler:
    def __init__(self, url, login_func=None, exclude_url_patterns=None, only_url_patterns=None):
        """
        :param url: The domain that is in this url will be used to fetch site.
        :param login_func: this is a function that gets as param driver and do all need before fetching stuff (like login)
        :param exclude_url_patterns: this pattens will be excluded when we try to find url with domain in self.domain
        :param only_url_patterns: this patterns will be used to filter from all others url
                if we like to fetch data only from some endpoint like {domain}/post/<post_number>
        """
        self.driver = webdriver.PhantomJS(executable_path=PHANTOMJS_PATH)
        Page.objects.get_or_create(url=url)
        self.domain = extract_domain(url)

        if only_url_patterns is None:
            only_url_patterns = []

        if exclude_url_patterns is None:
            exclude_url_patterns = []

        self.exclude_url_patterns = exclude_url_patterns
        self.only_url_patterns = only_url_patterns

        self.login_func = login_func
        if self.login_func:
            self.login_func(self.driver)

        self.previous_html = None

    def _remove_bad_tags(self):
        """
        This code removes from page "bad tags" such as a advertisement or js.
        """
        for tag in BAD_TAGS:
            self.driver.execute_script("""
                    var element = document.getElementsByTagName('""" + tag + """'), index;
                    for (index = element.length - 1; index >= 0; index--) {
                        element[index].parentNode.removeChild(element[index]);
                    }""")

    def _fetch_html_from_page(self, url):
        """
        Makes request, prepare html to save, fetch all urls from page
        :param url: url to fetch
        :return:
        """
        self.driver.get(url)
        self._remove_bad_tags()
        self._extract_and_save_all_urls_from_html()
        html = self.driver.page_source
        return html

    def _extract_and_save_all_urls_from_html(self):
        """
        Extracting all href params from tags and saves it to db.
        """
        hrefs = self.driver.find_elements_by_xpath("//*[@href]")
        for href in hrefs:
            url = check_url(href.get_attribute("href"))
            if url and len(url) < 100:
                Page.objects.get_or_create(url=url)

    def _filter_pages(self, pages):
        """
        :param pages:
        :return: filtered pages by patterns
        """
        result = []

        if self.only_url_patterns:
            for pattern in self.only_url_patterns:
                result.extend(list(pages.filter(url__iregex=pattern)))

        if self.exclude_url_patterns:
            for pattern in self.exclude_url_patterns:
                for page in list(pages.exclude(url__iregex=pattern)):
                    if page in result:
                        result.remove(page)

        return result

    def process(self):
        """
        Main process
        :return:
        """
        while True:
            pages = Page.objects.filter(url__icontains=self.domain, done=EMPTY)

            pages = self._filter_pages(pages)

            page = None
            for page in pages:
                if extract_domain(page.url) == self.domain:
                    break

            if not page:
                pages = Page.objects.filter(done=3)
                if not pages:
                    break
                else:
                    for pages in pages:
                        pages.done = 0
                        pages.save()

                    self.driver.close()
                    self.driver = webdriver.PhantomJS(executable_path=PHANTOMJS_PATH)

                    if self.login_func:
                        self.login_func(self.driver)

            else:
                page_id = page.id
                page.done = IN_PROCESS
                page.save()
                try:
                    pages = Page.objects.get(pk=page_id)
                    html = self._fetch_html_from_page(page.url)

                    if self.previous_html == html:
                        page.html_md5 = hashlib.md5(html.encode('utf-8')).hexdigest()
                        page.save_html(html)
                        page.done = INVALID
                        page.save()
                        file = open('logs', 'a')
                        file.write('Something goes wrong, trying again.\n')
                        file.close()
                        self.driver.close()
                        self.driver = webdriver.PhantomJS(executable_path=PHANTOMJS_PATH)
                        continue

                    page.html_md5 = hashlib.md5(html.encode('utf-8')).hexdigest()
                    page.save_html(html)
                    page.done = DONE
                    page.save()
                    self.previous_html = html
                except URLError as error:
                    page = Page.objects.get(pk=page_id)
                    page.done = INVALID
                    page.save()
                    self.driver.quit()
                    self.driver = webdriver.PhantomJS(executable_path=PHANTOMJS_PATH)
                except BaseException as error:
                    file = open('logs', 'a')
                    file.write(str(error) + ' : ' + str(pages.url) + ' : ' + 'with id ' + str(page_id) + '\n')
                    file.close()
                    pages = Page.objects.get(pk=page_id)
                    pages.done = INVALID
                    pages.save()

    def __del__(self):
        self.driver.close()
