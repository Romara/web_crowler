from server.celery import app


@app.task
def crawl_a_website(website_url, login_func=None, exclude_url_patterns=None, only_url_patterns=None):
    from .crowler_client import Crowler
    crowler = Crowler(website_url, login_func, exclude_url_patterns, only_url_patterns)
    crowler.process()
    del crowler



