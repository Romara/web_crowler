EMPTY = 0
IN_PROCESS = 1
DONE = 2
INVALID = 3

URL_STATUSES = (
    (0, 'EMPTY'),
    (1, 'IN_PROCESS'),
    (2, 'DONE'),
    (3, 'INVALID'),
)

PHANTOMJS_PATH = '/vagrant/server/node_modules/phantomjs-prebuilt/lib/phantom/bin/phantomjs'


BAD_FORMAT = [
    'mp3',
    'gif',
    'jpg',
    'png',
    'img',
    'css',
]


BAD_TAGS = [
    'yatag',
    'iframe',
    'noscript',
    'script',
]
