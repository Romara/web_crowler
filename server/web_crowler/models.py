from django.db import models
from django.utils.functional import cached_property

from .utils import extract_domain
from .constants import *

import os


class Page(models.Model):
    url = models.URLField(unique=True)
    html_md5 = models.CharField(blank=True, null=True, max_length=100)

    done = models.IntegerField(default=EMPTY)

    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    @cached_property
    def html(self):
        filename = 'pages/' + extract_domain(self.url) + '/' + str(self.created_at) + '/' + self.html_md5 + '.html'
        file = open(filename, 'r', encoding='utf-8')
        html = file.read()
        return html

    def save_html(self, html):
        if not self.url or not self.html_md5 or not self.created_at:
            raise BaseException('Save obj with html md5!')
        filename = 'pages/' + extract_domain(self.url) + '/' + str(self.created_at) + '/' + self.html_md5 + '.html'
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        file = open(filename, 'w', encoding='utf-8')
        file.write(html)
        file.close()

    def __str__(self):
        return self.url + ' from ' + str(self.created_at)

    class Meta:
        db_table = 'pages'
        verbose_name = 'page'
        verbose_name_plural = 'pages'

        unique_together = ('url', 'html_md5')


class Proxy(models.Model):
    proxy = models.CharField(max_length=30, unique=True)
    available = models.BooleanField(default=False)
    using = models.BooleanField(default=False)

    class Meta:
        db_table = 'proxies'
        verbose_name = 'proxy'
        verbose_name_plural = 'proxies'

    def __str__(self):
        return self.proxy
