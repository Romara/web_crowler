# Create your views here.
# from models import Page
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page
from django.core.paginator import Paginator

from habrahabr_ru.tasks import parse_habrahabr_posts
from habrahabr_ru.models import ParsedPage
from .utils import extract_domain
from .models import Page

# from .tasks import *


def new_view(request):
    # from web_crowler.models import Page
    # for page in Page.objects.all().iterator():
    #     try:
    #         Page.objects.get(url=page.url)
    #     except:
    #         pages = Page.objects.filter(url=page.url)
    #         i = 0
    #         for page in pages:
    #             i += 1
    #             if i == 1:
    #                 continue
    #             else:
    #                 page.delete()
    #
    # pages = Page.objects.filter(html_md5__in=['086707e4369f60afedcafb16050a7618', '608ecc5f90dff09fe7fa099c9eab6257'])
    # for page in pages.iterator():
    #     page.html_md5 = None
    #     page.done = 0
    #     page.save()
    #
    # crawl_a_website.apply_async(('https://habrahabr.ru/',), {'only_url_patterns': [r'habrahabr\.ru/post/\d+/$',
    #                                                                                r'habrahabr\.ru/company/[\w\-_]+/blog/\d+/$',
    #                                                                                r'habrahabr\.ru/users/[\w\-_]+/$',
    #                                                                                r'habrahabr\.ru/company/[\w\-_]+/profile/$'],
    #                                                          'exclude_url_patterns': [r'm\.habrahabr\.ru']})

    # from web_crowler.models import Page
    # page = Page.objects.get(id=92835)
    # parse_company_page(page)
    #
    parse_habrahabr_posts.apply_async()
    return render(request, 'base.html')


def show_db(request):
    if request.method == 'POST':
        search = request.POST.get('search', '')
        if extract_domain(search):
            page_log = Page.objects.filter(done=2, url=search).order_by('-updated_at')
            return render(request, 'web_crowler/db.html', {'pages': page_log})
        else:
            try:
                page_id = int(search)
                page = Page.objects.get(id=page_id)
                return render(request, 'web_crowler/db.html', {'pages': [page]})

            except ValueError:
                return render(request, 'web_crowler/db.html', {'errors': 'Search by id or url only!'})
    else:
        pages = Page.objects.filter(done=2)
        paginator = Paginator(pages, 20)
        page = request.GET.get('page', '')
        if page:
            try:
                list_of_pages = paginator.page(int(page))
            except ValueError:
                list_of_pages = paginator.page(1)
        else:
            list_of_pages = paginator.page(1)
        return render(request, 'web_crowler/db.html', {'pages': list_of_pages})


def get_page(request):
    page_id = request.GET.get('page_id')
    if not page_id:
        return redirect('/')
    try:
        page = Page.objects.get(id=page_id)
        return HttpResponse(page.html)
    except:
        return HttpResponse(content='No page in local db', status=404)


@cache_page(60 * 5)
def index(request):
    page_log = Page.objects.filter(done=2).order_by('-updated_at')[:20]
    pages_fetched = len(Page.objects.filter(done=2))
    url_fetched = len(Page.objects.all())
    pages_parsed = len(ParsedPage.objects.filter(status=1))
    memory = str(int(7190200 / 1024)) + 'MB'
    return render(request, 'web_crowler/index.html', {'pages': page_log, 'pages_parsed': pages_parsed, 'memory': memory,
                                                      'url_fetched': url_fetched, 'pages_fetched': pages_fetched})
