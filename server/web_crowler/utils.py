from django.template.defaultfilters import register
from tld import get_tld
from urllib.parse import urlparse

from .constants import BAD_FORMAT


def extract_domain(site_url):
    """
    :param site_url:
    :return: Extract domain from url.
    """
    result = get_tld(site_url, as_object=True, fail_silently=True)
    if result:
        return result.tld
    return None


def check_url(site_url):
    """
    :param site_url:
    :return: parsed and checked url
    return None if url have params or query. Just url for post request.
    """
    res = urlparse(site_url)
    if res.params or res.query:
        return None
    if res.path.split('.')[-1].lower() in BAD_FORMAT:
        return None
    if not res.scheme or not res.netloc:
        return None
    return res.scheme + '://' + res.netloc + res.path


@register.filter
def extract_domain_filter(url):
    return extract_domain(url)
