from django.http import HttpResponse
from django.shortcuts import render
from django.db.models import Q

from .models import *
from .constants import PER_PAGE


def can_be_int(string):
    try:
        int(string)
        return True
    except ValueError:
        return False


def index(request):
    return get_posts(request)


def get_posts(request):
    search = request.GET.get('search', '')
    page_num = request.GET.get('page', '')

    if page_num and can_be_int(page_num):
        page_num = int(page_num)
        if page_num > 0:
            prev_page = page_num - 1
        else:
            prev_page = None
    else:
        page_num = 0
        prev_page = None

    if search:
        post_list = Post.objects.filter(Q(title__icontains=search) | Q(post__icontains=search))[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
    else:
        post_list = Post.objects.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]

    if len(post_list) == PER_PAGE + 1:
        next_page = page_num + 1
    else:
        next_page = None
    if prev_page == 0:
        prev_page = '0'

    return render(request, 'habrahabr_ru/posts.html', {'posts': post_list[:PER_PAGE],
                                                       'prev_page': prev_page,
                                                       'next_page': next_page,
                                                       'page_num': page_num})


def get_post(request, post_id):
    post = Post.objects.get(id=post_id)
    return render(request, 'habrahabr_ru/post.html', {'post': post})


def get_user(request, username):
    user = User.objects.get(nickname=username)
    return render(request, 'habrahabr_ru/user.html', {'user': user})


def get_company(request, company_url_name):
    company = Company.objects.get(url_name=company_url_name)
    users = User.objects.filter(company=company)
    return render(request, 'habrahabr_ru/company.html', {'company': company, 'users': users})


def get_posts_by_tag(request, tag_type, tag_url_title):
    page_num = request.GET.get('page', '')

    if page_num and can_be_int(page_num):
        page_num = int(page_num)
        if page_num > 0:
            prev_page = page_num - 1
        else:
            prev_page = None
    else:
        page_num = 0
        prev_page = None

    search = request.GET.get('search', '')

    if not search:
        if tag_type.lower() == 'flow':
            flow = Flow.objects.get(url_title=tag_url_title)
            posts = Post.objects.filter(flow=flow)[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
        elif tag_type.lower() == 'tag':
            tag = Tag.objects.get(title=tag_url_title)
            posts = tag.post_set.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
        elif tag_type.lower() == 'hub':
            hub = Hub.objects.get(url_title=tag_url_title)
            posts = hub.post_set.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
        else:
            return HttpResponse(status=404)

    else:
        if tag_type.lower() == 'flow':
            flow = Flow.objects.get(url_title=tag_url_title)
            posts = Post.objects.filter(flow=flow).filter(Q(title__icontains=search) | Q(post__icontains=search))[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
        elif tag_type.lower() == 'tag':
            tag = Tag.objects.get(title=tag_url_title)
            posts = tag.post_set.filter(Q(title__icontains=search) | Q(post__icontains=search))[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
        elif tag_type.lower() == 'hub':
            hub = Hub.objects.get(url_title=tag_url_title)
            posts = hub.post_set.filter(Q(title__icontains=search) | Q(post__icontains=search))[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
        else:
            return HttpResponse(status=404)

    if len(posts) == PER_PAGE + 1:
        next_page = page_num + 1
    else:
        next_page = None
    if prev_page == 0:
        prev_page = '0'
    return render(request, 'habrahabr_ru/posts.html', {'posts': posts[:PER_PAGE],
                                                       'prev_page': prev_page,
                                                       'next_page': next_page,
                                                       'page_num': page_num})


def get_users(request):
    search = request.GET.get('search', '')
    page_num = request.GET.get('page', '')

    if page_num and can_be_int(page_num):
        page_num = int(page_num)
        if page_num > 0:
            prev_page = page_num - 1
        else:
            prev_page = None
    else:
        page_num = 0
        prev_page = None

    if search:
        users = User.objects.filter(Q(nickname__icontains=search) | Q(name__icontains=search))[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
    else:
        users = User.objects.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]

    if len(users) == PER_PAGE + 1:
        next_page = page_num + 1
    else:
        next_page = None

    if prev_page == 0:
        prev_page = '0'

    return render(request, 'habrahabr_ru/users.html', {'users': users[:PER_PAGE],
                                                       'search': search,
                                                       'prev_page': prev_page,
                                                       'next_page': next_page,
                                                       'page_num': page_num})


def get_companies(request):
    search = request.GET.get('search', '')
    page_num = request.GET.get('page', '')

    if page_num and can_be_int(page_num):
        page_num = int(page_num)
        if page_num > 0:
            prev_page = page_num - 1
        else:
            prev_page = None
    else:
        page_num = 0
        prev_page = None

    if search:
        companies = Company.objects.filter(name__icontains=search)[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]
    else:
        companies = Company.objects.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]

    if len(companies) == PER_PAGE + 1:
        next_page = page_num + 1
    else:
        next_page = None

    if prev_page == 0:
        prev_page = '0'

    return render(request, 'habrahabr_ru/companies.html', {'companies': companies[:PER_PAGE],
                                                           'search': search,
                                                           'prev_page': prev_page,
                                                           'next_page': next_page,
                                                           'page_num': page_num})


def get_hubs(request):
    page_num = request.GET.get('page', '')

    if page_num and can_be_int(page_num):
        page_num = int(page_num)
        if page_num > 0:
            prev_page = page_num - 1
        else:
            prev_page = None
    else:
        page_num = 0
        prev_page = None

    hubs = Hub.objects.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]

    if len(hubs) == PER_PAGE + 1:
        next_page = page_num + 1
    else:
        next_page = None

    if prev_page == 0:
        prev_page = '0'

    return render(request, 'habrahabr_ru/hubs.html', {'hubs': hubs[:PER_PAGE],
                                                      'prev_page': prev_page,
                                                      'next_page': next_page,
                                                      'page_num': page_num})


def get_tags(request):
    page_num = request.GET.get('page', '')

    if page_num and can_be_int(page_num):
        page_num = int(page_num)
        if page_num > 0:
            prev_page = page_num - 1
        else:
            prev_page = None
    else:
        page_num = 0
        prev_page = None

    tags = Tag.objects.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]

    if len(tags) == PER_PAGE + 1:
        next_page = page_num + 1
    else:
        next_page = None

    if prev_page == 0:
        prev_page = '0'

    return render(request, 'habrahabr_ru/tags.html', {'tags': tags[:PER_PAGE],
                                                      'prev_page': prev_page,
                                                      'next_page': next_page,
                                                      'page_num': page_num})


def get_flows(request):
    page_num = request.GET.get('page', '')

    if page_num and can_be_int(page_num):
        page_num = int(page_num)
        if page_num > 0:
            prev_page = page_num - 1
        else:
            prev_page = None
    else:
        page_num = 0
        prev_page = None

    flows = Flow.objects.all()[PER_PAGE * page_num:PER_PAGE * (page_num + 1) + 1]

    if len(flows) == PER_PAGE + 1:
        next_page = page_num + 1
    else:
        next_page = None

    if prev_page == 0:
        prev_page = '0'

    return render(request, 'habrahabr_ru/flows.html', {'flows': flows[:PER_PAGE],
                                                       'prev_page': prev_page,
                                                       'next_page': next_page,
                                                       'page_num': page_num})
