from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='habrahabr_index'),
    url(r'^posts$', get_posts, name='get_habrahabr_posts'),
    url(r'^post/(?P<post_id>[0-9]+)/$', get_post, name='get_post'),
    url(r'^user/(?P<username>[\w-]+)/$', get_user, name='get_user'),
    url(r'^company/(?P<company_url_name>[\w-]+)/$', get_company, name='get_company'),
    url(r'^hubs/$', get_hubs, name='get_hubs'),
    url(r'^tags/$', get_tags, name='get_tags'),
    url(r'^flows/$', get_flows, name='get_flows'),
    url(r'^companies/$', get_companies, name='get_companies'),
    url(r'^users/$', get_users, name='get_users'),
    url(r'^(?P<tag_type>[\w-]+)/(?P<tag_url_title>[ \w-]+)/$', get_posts_by_tag, name='get_posts_by_tag')
]
