# -*- coding: utf-8 -*-
import datetime
import re
from django.utils.timezone import utc
from django.template.defaultfilters import register


MONTHS = {
    u'января': 1,
    u'февраля': 2,
    u'марта': 3,
    u'апреля': 4,
    u'мая': 5,
    u'июня': 6,
    u'июля': 7,
    u'августа': 8,
    u'сентября': 9,
    u'ноября': 10,
    u'октября': 11,
    u'декабря': 12,
}


DAY_TYPES = {
    u'сегодня': 0,
    u'вчера': 1,
    u'позавчера': 2,
}


def habrahabr_date_parse(text_date, date_fetched):
    """
    Utils for parsing dates from habrahabr's pages.
    """
    try:
        search = re.search(r'(\w+)\s+(\S+)\s+(\w+)\s+\S\s+(\w+):(\w+)', text_date)
        if search:
            day = int(search.group(1))
            month = MONTHS[search.group(2)]
            year = int(search.group(3))
            hour = int(search.group(4))
            minute = int(search.group(5))
            return datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute, tzinfo=utc)

        search = re.search(r'(\w+)\s+(\S+)\s+\S\s+(\w+):(\w+)', text_date)
        if search:
            day = int(search.group(1))
            month = MONTHS[search.group(2)]
            hour = int(search.group(3))
            minute = int(search.group(4))
            year = date_fetched.year
            return datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute, tzinfo=utc)

        search = re.search(r'(\S+)\s+\S\s+(\w+):(\w+)', text_date)
        if search:
            day_balance = DAY_TYPES[search.group(1)]
            hour = int(search.group(2))
            minute = int(search.group(3))
            day = date_fetched.day - day_balance
            month = date_fetched.month
            year = date_fetched.year
            return datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute, tzinfo=utc)

        search = re.search(r'(\w+)\s+(\S+)\s+(\w+)', text_date)
        if search:
            day = int(search.group(1))
            month = MONTHS[search.group(2)]
            year = int(search.group(3))
            return datetime.datetime(year=year, month=month, day=day, tzinfo=utc)

    except BaseException as error:
        return None

    return None


def to_float(string, symbol_to_split=','):
    return float(str(string).replace(' ', '').replace('–', '-').replace(symbol_to_split, '.'))


@register.filter
def query_to_string(query):
    result = ''
    for item in query:
        if type(item).__name__ == 'User':
            result += '<a href="/habrahabr_ru/user/' + item.nickname + '">' + item.nickname + '</a>, '
        elif type(item).__name__ == 'Tag':
            result += '<a href="/habrahabr_ru/' + type(item).__name__ + '/' + item.title + '">' + item.title + '</a>, '
        else:
            result += '<a href="/habrahabr_ru/' + type(item).__name__ + '/' + item.url_title + '">' + item.title + '</a>, '
    return result[:-2]
