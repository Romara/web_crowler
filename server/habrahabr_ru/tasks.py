from .utils import habrahabr_date_parse, to_float
from web_crowler.constants import DONE
from bs4 import BeautifulStoneSoup
from .models import *
from server.celery import app
import re
from lxml.html import document_fromstring, unicode
from lxml import etree


def html_entity_to_unicode(text):
    """Converts HTML entities to unicode.  For example '&amp;' becomes '&'."""
    text = unicode(BeautifulStoneSoup(text))
    return text


def parse_post_page(page):
    """Parsing post page"""
    tree = document_fromstring(page.html)
    print(page.url)

    if not tree.xpath(".//*[(contains(@class,'layout__base'))]"):
        ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['error']})
        return

    title_element = tree.xpath(".//*[(contains(@class,'post__title'))]")[0]

    flow = title_element.xpath(".//*[(contains(@class,'post__flow'))]")

    if flow:
        flow_title = flow[0].text_content()
        flow_url_title = tree.xpath(".//*[(contains(@class,'post__flow'))]/@href")[0][27:-1]
        flow = Flow.objects.get_or_create(url_title=flow_url_title, defaults={'title': flow_title})[0]
        title = title_element.xpath('./span')[1].text_content()
    else:
        flow = None
        title = title_element.xpath('./span')[0].text_content()

    # post = tree.xpath(".//*[(contains(@class,'content html_format'))]")[0].text_content()
    post = html_entity_to_unicode(etree.tostring(tree.xpath(".//*[(contains(@class,'content html_format'))]")[0]))

    rating_element = tree.xpath(".//*[(contains(@class,'voting-wjt__counter-score js-score'))]")
    if rating_element:
        rating = to_float(rating_element[0].text_content())
    else:
        rating = 0

    author_nickname_element = tree.xpath(".//*[(contains(@class,'post-type__value post-type__value_author'))]")
    if author_nickname_element:
        author_nickname = author_nickname_element[0].text_content()[1:]
        author_name = tree.xpath(".//*[(contains(@class,'post-type__value post-type__value_author'))]/@title")
    else:
        author_nickname = tree.xpath(".//*[(contains(@class,'author-info__nickname'))]")[0].text_content()[1:]
        author_name_element = tree.xpath(".//*[(contains(@class,'author-info__name'))]/@title")
        if author_name_element:
            author_name = author_name_element
        else:
            author_name = ''

    author = User.objects.get_or_create(nickname=author_nickname, defaults={'name': author_name})[0]

    publish_date = habrahabr_date_parse(tree.xpath(".//*[(contains(@class,'post__time_published'))]")[0].text_content(),
                                        page.updated_at)

    company = None

    if tree.xpath(".//*[(contains(@class,'voting-wjt__counter-score js-karma_num'))]"):
        author_name_element = tree.xpath(".//*[(contains(@class,'author-info__name'))]")
        if author_name_element:
            author.name = author_name_element[0].text_content()
        author.status = str(tree.xpath(".//*[(contains(@class,'author-info__specialization'))]")[0].text_content()).strip()

        author_rating_element = tree.xpath(".//*[(contains(@class,'statistic__value statistic__value_magenta'))]")
        if author_rating_element:
            author_rating = author_rating_element[0].text_content()
            author.rating = to_float(author_rating)

        author_karma_element = tree.xpath(".//*[(contains(@class,'voting-wjt__counter-score js-karma_num'))]")
        if author_karma_element:
            author_karma = author_karma_element[0].text_content()
            author.karma = to_float(author_karma)

        author.save()
    else:
        company_name = tree.xpath(".//*[(contains(@class,'author-info__name'))]")[0].text_content()
        company_url_element = tree.xpath(".//*[(contains(@class,'author-info__name'))]")[0].xpath(".//@href")
        if company_url_element:
            company_url = company_url_element[0]
            company_url_name = re.search(r"company/([\w-]+)/", company_url).group(1)
            company = Company.objects.get_or_create(url_name=company_url_name, defaults={'name': company_name})[0]
            company_rating = tree.xpath(".//*[(contains(@class,'user-rating__value'))]")[0].text_content()
            company.rating = to_float(company_rating)
            company.status = tree.xpath(".//*[(contains(@class,'author-info__specialization'))]")[0].text_content().strip()
            company.save()

    post = Post.objects.update_or_create(page=page, defaults={'rating': rating, 'publish_date': publish_date,
                                                              'title': title, 'author': author, 'company': company,
                                                              'flow': flow, 'post': post})[0]

    tags = tree.xpath(".//*[(contains(@rel,'tag'))]")
    for tag in tags:
        tag_object = Tag.objects.get_or_create(title=tag.text_content())[0]
        post.tags.add(tag_object)

    hubs = tree.xpath(".//*[(contains(@class, 'hub '))]")
    for hub in hubs:
        hab_url_title_search = re.search(r"hub/([\w-]+)/", hub.xpath('.//@href')[0])
        if hab_url_title_search:
            hab_url_title = hab_url_title_search.group(1)
            hub_object = Hub.objects.get_or_create(url_title=hab_url_title, defaults={'title': hub.text_content()})[0]
            post.hubs.add(hub_object)

    post.save()

    ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['post']})


def parse_user_page(page):
    """Parsing user info page"""
    tree = document_fromstring(page.html)

    if not tree.xpath(".//*[(contains(@class,'layout__base'))]"):
        ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['error']})
        return

    author_nickname = tree.xpath(".//*[(contains(@class,'author-info__nickname'))]")[0].text_content()[1:]
    author_name_element = tree.xpath(".//*[(contains(@class,'author-info__name'))]")
    if author_name_element:
        author_name = author_name_element[0].text_content()
    else:
        author_name = None
    author_status = str(tree.xpath(".//*[(contains(@class,'author-info__specialization'))]")[0].text_content()).strip()

    author_rating_element = tree.xpath(".//*[(contains(@class,'statistic__value statistic__value_magenta'))]")
    if author_rating_element:
        author_rating = to_float(author_rating_element[0].text_content())
    else:
        author_rating = None

    author_karma_element = tree.xpath(".//*[(contains(@class,'voting-wjt__counter-score js-karma_num'))]")
    if author_karma_element:
        author_karma = to_float(author_karma_element[0].text_content())
    else:
        author_karma = None

    company_element = tree.xpath(".//*[(contains(@rel,'me'))]")
    if company_element:
        company_name = company_element[0].text_content()
        company_url_name = re.search(r"company/([\w-]+)/", company_element[0].xpath(".//@href")[0]).group(1)
        company = Company.objects.get_or_create(url_name=company_url_name, defaults={'name': company_name})[0]
    else:
        company = None

    about_element = tree.xpath(".//dd[(contains(@class,'summary'))]")
    if about_element:
        about = about_element[0].text_content().strip()
    else:
        about = None

    author = User.objects.get_or_create(nickname=author_nickname)[0]

    date_registered = habrahabr_date_parse('сегодня в 12:32', page.created_at)
    date_last_event = habrahabr_date_parse('сегодня в 12:34', page.created_at)

    info_lists = tree.xpath(".//dl")
    for info in info_lists:
        data = info.xpath(".//dt")
        if data:
            if u'Зарегистрирован' in data[0].text_content():
                date_registered = habrahabr_date_parse(info.xpath(".//dd")[0].text_content(), page.created_at)
            if u'Активность:' in data[0].text_content():
                date_last_event = habrahabr_date_parse(info.xpath(".//dd")[0].text_content(), page.created_at)
            if u'Откуда:' in data[0].text_content():
                for city in info.xpath(".//a"):
                    city_title = city.text_content()
                    city_url_title = re.search(r"/\?(\w+=\w+)", city.xpath(".//@href")[0]).group(1)
                    city = City.objects.get_or_create(url_title=city_url_title, defaults={'title': city_title})[0]
                    author.city.add(city)

    author.name = author_name
    author.rating = author_rating
    author.karma = author_karma
    author.status = author_status
    author.company = company
    author.about = about
    author.registered = date_registered
    author.last_event = date_last_event
    author.page = page

    favorite_companies_list = tree.xpath(".//*[(contains(@id, 'fav_companies_data_items'))]")
    if favorite_companies_list:
        favorite_companies_elements = favorite_companies_list[0].xpath(".//a")
        for favorite_company_element in favorite_companies_elements:
            company_url = favorite_company_element.xpath(".//@href")[0]
            company_url_name_element = re.search(r"company/([\w-]+)/", company_url)
            if company_url_name_element:
                company_url_name = company_url_name_element.group(1)
                company_name = favorite_company_element.text_content()
                company = Company.objects.get_or_create(name=company_name, url_name=company_url_name)[0]
                author.favorite.add(company)

    hubs_list = tree.xpath(".//*[(contains(@id, 'hubs_data_items'))]")
    if hubs_list:
        hubs_elements = hubs_list[0].xpath(".//a")
        for hub_element in hubs_elements:
            hub_url = hub_element.xpath(".//@href")[0]
            hub_url = re.search(r"hub/([\w-]+)/", hub_url)
            if hub_url:
                hub_url_name = hub_url.group(1)
                hub_name = hub_element.text_content()
                hub = Hub.objects.get_or_create(title=hub_name, url_title=hub_url_name)[0]
                author.hubs.add(hub)

    author.save()

    ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['user']})


def parse_company_page(page):
    """Parsing company info page"""
    tree = document_fromstring(page.html)

    if not tree.xpath(".//*[(contains(@class,'layout__base'))]"):
        ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['error']})
        return

    company_title_element = tree.xpath(".//*[(contains(@class,'author-info__name'))]")[0]
    company_url_title = re.search(r"/company/([\w-]+)/", company_title_element.xpath(".//@href")[0]).group(1)
    company_title = company_title_element.text_content()

    company_status = str(tree.xpath(".//*[(contains(@class,'author-info__specialization'))]")[0].text_content()).strip()

    company_rating = tree.xpath(".//*[(contains(@class,'user-rating__value'))]")[0].text_content()
    company_rating = to_float(company_rating)

    about_element = tree.xpath(".//dd[(contains(@class,'summary'))]")
    if about_element:
        about = about_element[0].text_content().strip()
    else:
        about = None

    date_registered = None

    info_lists = tree.xpath(".//dl")
    for info in info_lists:
        data = info.xpath(".//dt")
        if data:
            if u'регистрации:' in data[0].text_content():
                date_registered = habrahabr_date_parse(info.xpath(".//dd")[0].text_content(), page.created_at)

    company = Company.objects.get_or_create(url_name=company_url_title)[0]
    company.name = company_title
    company.status = company_status
    company.rating = company_rating
    company.about = about
    company.date_created = date_registered
    company.save()

    ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['company']})


def fetch_information_from_page(page):
    """With re find out what type of page it is"""
    if re.search(r'//habrahabr\.ru/post/\d+/$', page.url):
        return parse_post_page(page)
    elif re.search(r'//habrahabr\.ru/company/[\w\-_]+/blog/\d+/$', page.url):
        return parse_post_page(page)
    elif re.search(r'//habrahabr\.ru/users/[\w\-_]+/$', page.url):
        return parse_user_page(page)
    elif re.search(r'//habrahabr\.ru/company/[\w\-_]+/profile/$', page.url):
        return parse_company_page(page)
    else:
        ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['undefined']})


@app.task
def parse_habrahabr_posts():
    pages = Page.objects.filter(url__icontains='habrahabr.ru', done=DONE).exclude(parsedpage__status__in=[0, 1, 2, 3])
    for page in pages:
        # try:
        #     print(page.url)
        fetch_information_from_page(page)
        # except BaseException as error:
        #     print(str(error).encode('utf-8'))
        #     ParsedPage.objects.get_or_create(page=page, defaults={'status': HABRAHABR_PAGE_STATUSES['error']})
