from django.db import models

from .constants import HABRAHABR_PAGE_STATUSES
from web_crowler.models import Page


class Flow(models.Model):
    title = models.CharField(max_length=150)
    url_title = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'flows'
        verbose_name = 'Flow'


class Hub(models.Model):
    title = models.CharField(max_length=150)
    url_title = models.CharField(max_length=150, unique=True)

    rating = models.FloatField(default=0)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'hubs'
        verbose_name = 'Hub'


class Tag(models.Model):
    title = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'tags'
        verbose_name = 'tag'


class City(models.Model):
    title = models.CharField(max_length=150)
    url_title = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'cities'
        verbose_name = 'City'
        verbose_name_plural = 'Cities'


class Company(models.Model):
    page = models.ForeignKey(Page, null=True, blank=True)

    name = models.CharField(max_length=150)
    url_name = models.CharField(max_length=150)

    status = models.CharField(max_length=150, null=True, blank=True)

    site_url = models.URLField(null=True, blank=True)

    rating = models.FloatField(default=0, null=True, blank=True)

    about = models.TextField(null=True, blank=True)

    date_created = models.DateField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'companies'
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'


class User(models.Model):
    page = models.ForeignKey(Page, null=True, blank=True)

    name = models.CharField(max_length=150, null=True, blank=True)
    nickname = models.CharField(max_length=150, unique=True)

    status = models.CharField(max_length=150, null=True, blank=True)

    favorite = models.ManyToManyField(Company, related_name='primary_company')

    city = models.ManyToManyField(City)
    hubs = models.ManyToManyField(Hub)

    company = models.ForeignKey(Company, null=True, blank=True)

    rating = models.FloatField(default=0, null=True, blank=True)
    karma = models.FloatField(default=0, null=True, blank=True)

    about = models.TextField(null=True, blank=True)

    registered = models.DateTimeField(null=True, blank=True)
    last_event = models.DateTimeField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nickname

    class Meta:
        db_table = 'users'
        verbose_name = 'User'


class Post(models.Model):
    page = models.OneToOneField(Page)

    title = models.CharField(max_length=300)
    post = models.TextField()

    tags = models.ManyToManyField(Tag)
    hubs = models.ManyToManyField(Hub)

    flow = models.ForeignKey(Flow, null=True, blank=True)

    rating = models.FloatField()
    publish_date = models.DateTimeField(null=True, blank=True)

    author = models.ForeignKey(User)
    company = models.ForeignKey(Company, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'posts'
        verbose_name = 'Post'


class ParsedPage(models.Model):
    page = models.OneToOneField(Page)
    status = models.IntegerField()

    def __str__(self):
        return self.page + ' with status: ' + HABRAHABR_PAGE_STATUSES[self.status]

    class Meta:
        db_table = 'parsed_pages'
        verbose_name = 'Parsed page'
        verbose_name_plural = 'Parsed pages'
