#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y python-software-properties
sudo apt-add-repository "ppa:fkrull/deadsnakes"
sudo apt-get update
sudo apt-get install -y python3.5
sudo apt-get install -y python-pip python3.5-dev build-essential
sudo apt-get install -y rabbitmq-server
sudo pip install --upgrade pip
sudo pip install --upgrade virtualenv
sudo pip install --upgrade virtualenvwrapper
sudo apt-get install -y libpq-dev postgresql postgresql-contrib

#sudo apt-get install -y libicu48
#sudo apt-get install -y build-essential npm nodejs
#sudo apt-get install -y phantomjs
# npm install phantomjs-prebuilt
